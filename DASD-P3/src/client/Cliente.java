package client;

import java.rmi.Naming;
import rmi.IServer;

public class Cliente {

	public static void main(String[] args) {
		
		try {
			// Le decimos que use el archivo "rmi.policy" para permisos.
			// Otorgandole todos
			System.setProperty("java.security.policy", "rmi.policy");

			// Inicializamos el Security Manager
			System.setSecurityManager(new SecurityManager());

			// Obtenemos una instancia usando el url.
			IServer s = (IServer) Naming.lookup("rmi://localhost:2320/Server");

			// Invocamos el metodo que se ejecutara remotamente
			s.sayHello("Ernesto Mate Ensalsa");

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
