package rmi;

import java.rmi.RemoteException;

import java.rmi.Remote;

/**
 * 
 * En la interfaz definimos cuales son los metodos que van a poder ser accedidos
 * remotamente
 *
 */

public interface IServer extends Remote {

	/**
	 * 
	 * Por este metodo vamos a enviar al Servidor una cadena para que la imprima
	 * 
	 * @param text
	 *            Texto que vamos a enviar
	 * 
	 * @throws RemoteException
	 *             Si algo falla tira un RemoteException
	 * 
	 */

	void sayHello(String text) throws RemoteException; // Es importante ponerle el
														// throws RemoteException

}
