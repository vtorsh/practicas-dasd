package server;

import java.rmi.*;
import rmi.IServer;

import java.rmi.server.*;

/**
 * 
 * Esta es la implementacion de nuestro servidor
 * 
 * @author Ernesto Maldonado Thomas
 * 
 */

public class Server extends UnicastRemoteObject implements IServer {

	/**
	 * 
	 * Al heredar de UnicastRemoteObject nos obliga a poner
	 * 
	 * este constructor
	 * 
	 * @throws RemoteException
	 * 
	 */

	public Server() throws RemoteException {

		super();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see IServer#sayHello(java.lang.String)
	 * 
	 */

	public void sayHello(String text) throws RemoteException {

		System.out.println("Hello from: " + text);

	}

}
