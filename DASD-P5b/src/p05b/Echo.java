package p05b;

import java.io.*;
import java.net.*;

public class Echo {
	private static EchoObject ss;

	public static void main(String[] args) {
		/* COMPLETAR: Cree una instància de ss */
		/* 1 */
		ss = new EchoObject();
		/* 1 */
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter stdOut = new PrintWriter(System.out);
		String input, output;
		
		try {
			stdOut.print("> ");
			stdOut.flush();
			/*
			 * COMPLETAR: En bucle infinit, llegir del teclat, invocar ss.echo i
			 * imprimir
			 */
			/* 2 */
			for(;;) {
				input = stdIn.readLine();
				output = ss.echo(input);
				System.out.println(output);
			}
			/* 2 */
		} catch (IOException e) {
		}
	}

}
