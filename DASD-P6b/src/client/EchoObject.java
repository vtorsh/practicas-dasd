package client;

import java.io.*;
import java.text.*;
import java.util.*;
import compute.Task;

public class EchoObject implements Task<String>, Serializable {
	private static final long serialVersionUID = 1L;
	String myURL = "localhost";

	public String execute(String input) {
		Date h = new Date();
		String data = DateFormat.getTimeInstance(3, Locale.FRANCE).format(h);
		String ret = myURL + ":" + data + "> " + input;

		try {
			Thread.sleep(3000);
			ret = ret + " (amb retard de 3 segons)";
		} catch (InterruptedException e) {
		}
		return ret;
	}

}