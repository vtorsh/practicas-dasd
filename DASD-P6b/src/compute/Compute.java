package compute;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Compute extends Remote {
	// Object executeTask(Task t) throws RemoteException;
	// loadTask: Carregar una nova task al ComputeEngine. No executar-la
	void loadTask(Task<?> t) throws RemoteException;

	// executeTask: Executar una tasca prèviament carregada amb loadTask
	// admetent com arguments els donats en arg i tornant el resultat de Task
	// com resultat de executeTask
	Object executeTask(Object arg) throws RemoteException;
}