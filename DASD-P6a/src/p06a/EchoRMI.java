package p06a;

import java.io.*;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class EchoRMI {
	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Us: EchoRMI host");
			System.exit(1);
		}
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter stdOut = new PrintWriter(System.out);
		String input, output;
		
		try {
			/* COMPLETAR: Registrar l'objecte RMI amb el nom echo */
			/* 1 */
			
			EchoInt obj= (EchoInt) Naming.lookup("rmi://localhost/echo");
			
			/* 1 */
			stdOut.print("> ");
			stdOut.flush();
			while ((input = stdIn.readLine()) != null) {
				/* COMPLETAR: Invocar al'objecte RMI */
				/* 2 */
				output= obj.echo(input);
				/* 2 */
				stdOut.println(output);
				stdOut.print("> ");
				stdOut.flush();
			}
		} catch (Exception e) {
			System.out.println("Error en client echo RMI: " + e.getMessage());
		}
	}
}
