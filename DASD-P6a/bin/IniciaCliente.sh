#!/bin/bash

if [ $# -lt 1 ]
then
	echo "Parametros incorrectos...  ./IniciaCliente.sh localhost"
	exit	
fi

p=`pwd`
rm client.policy
echo "grant codeBase \"file://$p\" {" > client.policy
echo "permission java.security.AllPermission;" >> client.policy
echo "};" >> client.policy

java -cp . -Djava.rmi.server.codebase=file://$p -Djava.security.policy=client.policy p06a.EchoRMI $1
