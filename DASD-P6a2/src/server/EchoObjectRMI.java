package server;

import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import rmi.EchoInt;
import rmi.EchoObject;

public class EchoObjectRMI implements EchoInt {
	protected EchoObjectRMI() {
		super();
	}

	private static EchoObject eo = new EchoObject();

	public String echo(String input) {
		return eo.echo(input);
	}

	public static void main(String[] args) {
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
		}
		try {
			Registry registry = LocateRegistry.getRegistry();
			EchoInt stub = (EchoInt) UnicastRemoteObject.exportObject(new EchoObjectRMI(), 0);
			registry.rebind("echo", stub);
		} catch (RemoteException e) {
			System.err.println("Error ocorregut en EchoObjectRMI");
			e.printStackTrace();
			System.exit(-1);
		}
		System.out.println("Servidor echo rmi en marxa");
	}
}
