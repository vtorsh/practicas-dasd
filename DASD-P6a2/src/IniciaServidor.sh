#!/bin/bash

if [ $# -lt 1 ]
then
	echo "Parametros incorrectos...  ./IniciaServidor.sh localhost"
	exit	
fi

rmiregistry &
p=`pwd`
rm server.policy
echo "grant codeBase \"file://$a\" {" > server.policy
echo "permission java.security.AllPermission;" >> server.policy
echo "};" >> server.policy

java -cp . -Djava.rmi.server.codebase=file://$p -Djava.rmi.server.hostname=$1 -Djava.security.policy=server.policy server.EchoObjectRMI
