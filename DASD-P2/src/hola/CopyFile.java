package hola;
import java.io.*;

public class CopyFile {
  public static void main (String[] args) throws java.io.IOException {
    int count= 0;
    String filenameorg= null;
    String filenamedst= null;
    InputStream is= null;
    OutputStream os= null;
    byte[] buffer= new byte[256];
    if (args.length < 2) {
      System.out.println("Usage: CopyFile fileorg filedst");
      System.exit(1);
    }
/* COMPLETAR: Cree una instància de FileInputStream denominada is per a llegir
 * del fitxer passat a args[0] i una instància de FileOutputStream denominada
 * os per a escriure al fitxer passat com args[1]
 */
/* 1 */
    is= new FileInputStream(args[0]);
    os= new FileOutputStream(args[1]);
    
  	filenameorg = args[0];
  	filenamedst = args[1];
/* 1 */
    while ((count= is.read(buffer)) != -1) os.write(buffer,0,count);
    System.out.println("Copied " + filenameorg + " to " + filenamedst);
    
    is.close();
    os.close();
  }
}
