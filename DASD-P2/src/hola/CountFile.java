package hola;

import java.io.*;

public class CountFile {
	public static void main (String[] args)
      throws java.io.IOException, java.io.FileNotFoundException {
    int count= 0;
    InputStream is= null;
    String filename;
    if (args.length >= 1) {
/* COMPLETAR: is= Cree una instància de FileInputStream
 * per a llegir del fitxer passat com argument a args[0]
 */
/* 1 */
    	is= new FileInputStream(args[0]);
/* 1 */
    	filename= args[0];
    } else {
    	is= System.in;
    	filename= "Input";
    }
/* COMPLETAR: Utilitze amb while (...) count++;
 * un mètode de FileInputSream per a llegir un caracter
 */
/* 2 */
    //char lectura;
    while(is.available() != 0) {
    	is.skip(1);
    	//c = (char) is.read();
    	//System.out.println(c);
    	count++;
    }
/* 2 */
    is.close();
	System.out.println(filename + " has " + count + " chars.");
  }
}
