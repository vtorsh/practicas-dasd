package hola;
import java.text.*;
import java.util.*;

class Rellotge extends Thread {
  int compte=0;
  public Rellotge(String nom, int compte) {
    super(nom); this.compte= compte;
  }
  public void start() {
    Calendar cal= Calendar.getInstance();
    cal.add(Calendar.DAY_OF_YEAR,2);
    Date data= cal.getTime();
    System.out.println(getName() + "-> " +
      "La data és ara: "+ data.toString());
    System.out.println(getName() + "-> " +
      DateFormat.getTimeInstance(3,Locale.FRANCE).format(data) +
      " Falten " + compte + " segons per a l'alarma");
    super.start();
  }
  public void run() {
    for (int i= 1; i <= compte; i++) {
/* COMPLETAR: Provoque un retardament de 1000 milisegons */
/* 1 */
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
/* 1 */
      System.out.println(getName() + " : " + (compte - i));
    }
    System.out.println(getName() + ": Riiinnnng!!!");
  }
}

public class Rellotges {
  public static void main(String[] args){
/* COMPLETAR: Cree dues instàncies de la classe Rellotge
 * amb comptes de 10 i 15, per exemple
 */
/* 2 */
	  Rellotge reloj1= new Rellotge("uno", 10);
	  Rellotge reloj2= new Rellotge("dos", 15);
	  reloj1.start();
	  reloj2.start();
/* 2 */
  }
}
