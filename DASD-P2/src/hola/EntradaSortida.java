package hola;
import java.io.*;

public class EntradaSortida{
  public static void main(String args[]) throws IOException {
    int j;
    byte[] buffer= new byte[80];
    String filename, filename2;
    float f1= (float) 3.1416;
    float f2= 0;
    try {
      // E/S amb InputStream i OutputStream
      System.out.println("Teclege una cadena");
      j= System.in.read(buffer);
      System.out.print("La cadena: ");
      System.out.write(buffer,0,j);
      // Conversi� de la cadena de bytes a cadena de caracters (2 bytes)
      String tira= new String(buffer,0,j);
      System.out.println("Altra vegada la cadena: " + tira);
      // E/S amb BufferedReader y PrintWriter
      // Convenient amb cadenes de caracters (1 caracter = 2 bytes)
      BufferedReader stdIn= new BufferedReader(new InputStreamReader(System.in));
      PrintWriter stdOut= new PrintWriter(System.out);
      // E/S amb InputStream i OutputStream
/* COMPLETAR: Llegir un enter per teclat i escriure'l en pantall */
/* 1 */
      int x;
      System.out.println("Escribe un entero: ");
      x= System.in.read(buffer);
      System.out.write(buffer, 0, x);
      System.out.print("\n");
/* 1 */
      // E/S amb BufferedReader i PrintWriter
      // Convenient amb cadenes de caracters (1 caracter = 2 bytes)
      System.out.println("Teclege un nom per a un fitxer");
/* COMPLETAR: Llegir una cadena per al nom del fitxer des del teclat
 * emmagamatzemar-la a la variable filename, i mostrar la variable per pantalla
 */
/* 2 */
      filename= stdIn.readLine();
      stdOut.println("Nombre del fichero: " + filename);
/* 2 */
      // E/S amb fitxers i floats en format num�ric
/* COMPLETAR: Escriga un float al fitxer filename (en format binari)
 * Llija el float que ha escrit al fitxer filename i mostre'l per pantalla
 * AJUDA: Mire el codi de m�s avall, que �s paregut per� en format de text
 */
/* 3 */
    		  
	  int intBits = Float.floatToIntBits(f1); 
      String binaryString = Integer.toBinaryString(intBits);
      System.out.println("Float en binario:" + binaryString);
	  DataOutputStream fout;
      fout= new DataOutputStream(new FileOutputStream(filename));
      fout.writeUTF(binaryString);
      
      DataInputStream fin;
      fin= new DataInputStream(new FileInputStream(filename));
      String res= fin.readUTF();
      
      intBits = Integer.parseInt(res, 2);
      float resFloat = Float.intBitsToFloat(intBits);
      
      System.out.println("Float leido del archivo: " + resFloat);
     /* DataOutputStream fout;
      fout= new DataOutputStream(new FileOutputStream(filename));
      fout.writeFloat(f1);
      
      DataInputStream fin;
      fin= new DataInputStream(new FileInputStream(filename));
      Float res= fin.readFloat();
      System.out.println("Float leido del archivo: " + res);
      
      //fout.writeBytes(new Float(f1).toString()+"\n");
      //f2= Float.valueOf(fin.readLine()).floatValue();
      */
/* 3 */
      // E/S amb fitxers i floats en format de text
      filename2=filename + ".txt";
      System.out.println("Fitxer: "+filename2);
      //fout= new DataOutputStream(new FileOutputStream(filename2));
      //fin= new DataInputStream(new FileInputStream(filename2));
      //fout.writeBytes(new Float(f1).toString()+"\n");
      //f2= Float.valueOf(fin.readLine()).floatValue();
      PrintWriter fout2= new PrintWriter(new FileOutputStream(filename2));
      BufferedReader fin2= new BufferedReader(new InputStreamReader(new FileInputStream(filename2)));
      fout2.println(new Float(f1).toString()); fout2.flush();
      //System.out.println(fin2.readLine());
      f2= Float.valueOf(fin2.readLine()).floatValue();
      System.out.println("Escrit i llegit el float: " +f2+ " del fitxer: " +filename2);
      fout.close();
      fin.close();
      fout2.close();
      fin2.close();
    } catch (IOException e) {
      System.out.println("Error en E/S");
      System.exit(1);
    }
  }
}
