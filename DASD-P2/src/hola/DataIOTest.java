/*
    Example from Sun Java I/O tutorial
    
    Modified to remove deprecated method: readLine(), and,
        provide a cleaner looking display
    Probably a better way to handle the DecimalFormat values;
        I'm creating to many unnecessary references
        but it works for this simple example
 */
package hola;
import java.io.*;
import java.text.DecimalFormat;     // to format numbers

/* Es tracta d'escriure en un fitxer amb format les línies amb valors passats
 * més avall de preu unitari, unitats i descripció
 * i després cal llegir aquestes línies del fitxer i treure-les per pantalla
 * Nota: Les dades numériques deuen escriure's com "números" i no com cadenes
 * mentre els Strings han d'escriure's com cadenes de bytes i no com cadenes
 * de caracters (1 caracter = 2 bytes)
 */

public class DataIOTest {
  public static void main(String[] args) throws IOException {
    // write the data out
/* COMPLETAR: DataOutputStream out= Instancie un objecte de tipus
 * DataOutputStream per a escriure al fitxer invoice.txt
 */
/* 1 */
	  FileOutputStream fileOut=new FileOutputStream("invoice.txt");
	  DataOutputStream out= new DataOutputStream(fileOut);
/* 1 */
    double[] prices= { 19.99, 9.99, 15.99, 3.99, 4.99 };
    int[] units= { 12, 8, 13, 29, 50 };
    String[] descs = { "Java T-shirt",
                       "Java Mug",
                       "Duke Juggling Dolls",
                       "Java Pin",
                       "Java Key Chain" }; 
    for (int i= 0; i < prices.length; i++) {
      out.writeDouble(prices[i]);
      out.writeChar('\t');
      out.writeInt(units[i]);
      out.writeChar('\t');
      out.writeChars(descs[i]);
      out.writeChar('\n');
    }
    out.close();
    // read it in again
/* COMPLETAR: DataInputStream in= Instancie un objecte de tipus
 * DataInputStream per a llegir del fitxer invoice.txt
 */
/* 2 */
    FileInputStream fileIn=new FileInputStream("invoice.txt");
    DataInputStream in = new DataInputStream(fileIn);
/* 2 */
    double price;
    int unit;
    String desc = "";
    double total= 0.0;
    DecimalFormat df= new DecimalFormat("###.##");   
    System.out.println("You've ordered:\n");      
    try {
    	int  x = 0;
    	while (true) {
/* COMPLETAR: Llegir el primer double del fitxer sobre la variable price */
/* 3 */
    	  price = in.readDouble();
/* 3 */
        in.readChar();       // throws out the tab
/* COMPLETAR: Llegir el valor int següent del fitxer sobre la variable unit */
/* 4 */
        unit = in.readInt();
/* 4 */
        in.readChar();       // throws out the tab
        // replace deprecated method
        //     desc= in.readLine();
        // with readChar()
/* COMPLETAR: Llegir la cadena següent spbre la variable desc sense fer ús
 * de in.readLine, ja en estat "deprecated"
 */
/* 5 */

        x++;
        char c;
        do { 
            c = in.readChar();
            if (c != '\n') 
            	desc += c;
         } while(c != '\n');
        
/* 5 */
        // replaced code to display results
        printInColumn(Integer.toString(unit), 10, 1);
        printInColumn(desc, 25, 0);
        printInColumn(Double.toString(price), 10, 2);
        System.out.println();                          
        total= total + unit * price;
      }
    } catch (EOFException e) {}    
    System.out.println("\nFor a TOTAL of: $" + df.format(total));
    in.close();
  }    
  // code from Java Class Libraries: 2nd edition, p.636   
  // modified to determine if value is int or double
  static void printInColumn(String str, int col, int flag) {
    String s;
    int length;
    DecimalFormat dfRnd= new DecimalFormat("#00.##");    
    DecimalFormat dfInt= new DecimalFormat("00");           
    switch (flag) {
      case 1:         // argument is an int
        s= "\t" + dfInt.format(Integer.parseInt(str));
        length= s.length();       
        break;
      case 2:         // argument is a double
        s= dfRnd.format(Double.parseDouble(str));
        length= s.length();
        break;
      default:
        s= str;
        length= str.length();
    }
    System.out.print(s);    
    for (int p= length; p < col; ++p) {
      System.out.print(" ");
    }
  }
}
