package hola;
import java.io.*;

class Punt {
  public int x= 0;
  public int y= 0;

  public Punt(int x, int y) {
    this.x= x;
    this.y= y;
  }
}

class Rectangle {
  protected Punt origen;
  protected int ample= 0;
  protected int alt= 0;
  private static String nomClasse ="Rectangle";

  public Rectangle(int origenx, int origeny, int ample, int alt) {
    origen= new Punt(origenx, origeny);
    this.ample= ample; this.alt= alt;
  }
  public Rectangle(Punt p, int ample, int alt) {
    origen= p;
    this.ample= ample; this.alt= alt;
  }
  public Rectangle(int ample, int alt) {
    origen= new Punt(0,0);
    this.ample= ample; this.alt= alt;
  }
  public Rectangle() {
    origen= new Punt(0,0);
    this.ample= 0; this.alt= 0;
  }

  public int ample() {
    return ample;
  }
  public int alt() {
    return alt;
  }
  public int area() {
    return (ample*alt);
  }
  public void menejar(int amunt, int dreta) {
    origen.x+= amunt; origen.y+= dreta;
  }
  public String toString() {
    return "(Origen: {" + Integer.toString(origen.x) +
           "," + Integer.toString(origen.y) +
           "}, Final: {" + Integer.toString(origen.x+ample) +
           "," + Integer.toString(origen.y+alt) + "})";
  }
  public static String nom(){
    return nomClasse;
  }
  protected void finalize() throws Throwable {
     origen= null;
     super.finalize();
  }
}

/* COMPLETAR: Implemente una classe RectangleColor, basada en Rectangle
 * amb un nou atribut color
 */
class RectangleColor extends Rectangle {
	protected int color= 7;
	
	public RectangleColor(int origenx, int origeny, int ample, int alt, int color) {
		super.origen= new Punt(origenx, origeny);
		super.ample= ample;
		super.alt= alt;
		this.color = color;
	  }
	
	public RectangleColor(Punt origen, int ample, int alt, int color) {
		super.origen= this.origen;
		super.ample= ample;
		super.alt= alt;
		this.color = color;
	  }
	
	public int color() {
	    return color;
	  }


  public String toString() {
/* COMPLETAR: Sobrecarregue aquest mètode per a que incloga també el color */
/* 2 */
    return "(Origen: {" + Integer.toString(origen.x) +
           "," + Integer.toString(origen.y) +
           "}, Final: {" + Integer.toString(origen.x+ample) +
           "," + Integer.toString(origen.y+alt) +
           "}, Color: " + Integer.toString(color) + "})";
/* 2 */
  }
}

class QuadratColor extends RectangleColor {
  public QuadratColor(Punt p, int costat, int color){
    super(p,costat,costat,color);
  }
}

public class Objectes{
  static Rectangle R1= new Rectangle(1,1,7,8);
  static Rectangle R2= new Rectangle(new Punt(2,2),7,8);
  static Rectangle R3 ;
  static RectangleColor RC;
  static QuadratColor Q;

  public static void main(String args[]) throws IOException {
    if (args.length < 4) {
      System.out.println("Ús: Objectes origen-x origen-y ample alt");
      System.exit(1);
    }

    int[] i= new int[4];
    int j= 0;

    for (j= 0; j < i.length; j++) {
       i[j]= Integer.parseInt(args[j]);
    }
    R3= new Rectangle(i[0],i[1],i[2],i[3]);
/* COMPLETAR: RC= Cree una instàmcia de RectangleColor RC 
 * que afegisca a R3 l'atribut color
 * Q= Cree un QuadratColor amb origen al punt (2,2), alt 5 i ample 25
 */
/* 3 */
    RC= new RectangleColor(i[0],i[1],i[2],i[3], 17);
    
    Punt p= new Punt(2,2);
    Q= new QuadratColor(p, 4, 8);
/* 3 */

    System.out.println("Nom de la classe: " + Rectangle.nom());
    System.out.println("Nom de la classe de R1: " + R1.getClass().getName());
    System.out.println("R3: " + R3.toString());
    System.out.println("RC: " + RC.toString());

/* COMPLETAR: Invoque el mètode menejar(10,10) sobre R3 i el mètode toString
 * sobre R3 i RC visualitzant per pantalla el resultat.
 * EXPLIQUE: S'ha "menejat" R3? I RC? Devien haver-se menejat?
 */
/* 4 */
R3.menejar(10, 10);
R3.toString();
RC.toString();

/* 4 */

    System.out.println("Àrea de R3: " + R3.area());
    System.out.println("Q: " + Q.toString());
    System.out.println("Àrea de Q: " + Q.area());
  }
}
