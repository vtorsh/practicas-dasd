package p05e;

import java.net.*;
import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

public class EchoObjectStubT implements EchoInt {
	private Socket echoSocket = null;
	private PrintWriter os = null;
	private BufferedReader is = null;
	private String host = "localhost";
	private int port = 7;
	private String output = "Error";
	private boolean connectat = false;
	Timeout tout = null;

	public void setHostAndPort(String host, int port) {
		this.host = host;
		this.port = port;
		tout = new Timeout(10, this);
	}

	public String echo(String input) throws java.rmi.RemoteException {
		connecta();
		if (echoSocket != null && os != null && is != null) {
			try {
				os.println(input);
				os.flush();
				output = is.readLine();
			} catch (IOException e) {
				System.err.println("Excepció llegint/escrivint socket");
			}
		}
		programaDesconnexio();
		return output;
	}

	private synchronized void connecta() throws java.rmi.RemoteException {
		try {
			if (!connectat) {
				echoSocket = new Socket(host, port);
				is = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
				os = new PrintWriter(echoSocket.getOutputStream());
				connectat = true;
				System.out.println("Connectat al server");
				/*
				 * COMPLETAR: Afegisca el codi que considere necessari per
				 * completar el if
				 */
				/* 1 */
			}
			/* 1 */
		} catch (Exception e) {
			throw new java.rmi.RemoteException();
		}
	}

	private synchronized void desconnecta() {
		try {
			os.close();
			is.close();
			echoSocket.close();
			connectat = false;
			System.out.println("Desconnectat del server");
		} catch (IOException e) {
			System.err.println("Error tancant socket");
		}
	}

	private synchronized void programaDesconnexio() {
		tout.start();
	}

	class Timeout {
		Timer timer;
		EchoObjectStubT stub;
		int segons;
	    TimeoutTask finTimer;
		boolean ftemp;

		public Timeout(int segons, EchoObjectStubT stub) {
			this.segons = segons;
			this.stub = stub;
		}

		/* COMPLETAR: Les línies de start, cancel i del run de TimeoutTask */
		public void start() {
			/* 2a */
			if (ftemp) {
				cancel();
			} else {
				ftemp = true;
			}
			finTimer = new TimeoutTask();
			timer.schedule(finTimer, segons * 1000, 1000);
			/* 2a */
		}

		public void cancel() {
			/* 2b */
			timer.cancel();
			/* 2b */
		}

		class TimeoutTask extends TimerTask {
			public void run() {
				/* 2c */
				ftemp = false;
				desconnecta();
				/* 2c */
			}
		}
	}
}
