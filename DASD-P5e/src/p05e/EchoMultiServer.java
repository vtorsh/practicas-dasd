package p05e;

import java.net.*;
import java.io.*;

public class EchoMultiServer {
	private static ServerSocket serverSocket = null;

	public static void main(String[] args) {
		try {
			serverSocket = new ServerSocket(4000);
		} catch (IOException e) {
			System.out.println("EchoMultiServer no escolta pel port 4000, " + e.toString());
			System.exit(1);
		}
		System.out.println("EchoMultiServer escoltant pel port 4000");
		boolean escoltant = true;
		while (escoltant) {
			/*
			 * COMPLETAR: Accepte una nova connexi� i genere un socket que la
			 * gestione
			 */
			/* 1 */
			
			try {
				Socket newSocket = serverSocket.accept();
				new EchoMultiServerThread(newSocket).start();
			} catch (IOException e) {
				System.out.println("Error creando newSocket " + e.toString());
			}

			/* 1 */
		}
		try {
			serverSocket.close();
		} catch (IOException e) {
			System.err.println("Error tancant socket server" + e.getMessage());
		}
	}
}

class EchoMultiServerThread extends Thread {
	private EchoObject eo;
	private Socket clientSocket = null;
	private String myURL = "localhost";
	private BufferedReader is = null;
	private PrintWriter os = null;
	private String inputline = new String();

	EchoMultiServerThread(Socket socket) {
		super("EchoMultiServerThread");
		clientSocket = socket;
		eo = new EchoObject();
		try {
			is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			os = new PrintWriter(clientSocket.getOutputStream());
		} catch (IOException e) {
			System.err.println("Error enviant/rebent: " + e.getMessage());
			e.printStackTrace();
		}
		try {
			myURL = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			System.out.println("Host desconegut: " + e.toString());
			System.exit(1);
		}
	}

	public void run() {
		try {
			while ((inputline = is.readLine()) != null) {
				/*
				 * COMPLETAR: Invocar l'objecte i tornar la resposta pel socket
				 */
				/* 2 */
				
				os.println( eo.echo(inputline) );
		        os.flush();
		        
				/* 2 */
			}
			os.close();
			is.close();
			clientSocket.close();
		} catch (IOException e) {
			System.err.println("Error enviant/rebent: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
