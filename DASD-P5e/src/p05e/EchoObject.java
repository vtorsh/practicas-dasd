package p05e;

import java.net.*;
import java.io.*;
import java.text.*;
import java.util.*;

public class EchoObject implements EchoInt {
	String myURL = "localhost";

	public String echo(String input) throws java.rmi.RemoteException {
		Date h = new Date();
		String data = DateFormat.getTimeInstance(3, Locale.FRANCE).format(h);
		String ret = myURL + ":" + data + "> " + input;

		try {
			Thread.sleep(3000);
			ret = ret + " (amb retard de 3 segons)";
		} catch (InterruptedException e) {
		}

		return ret;
	}
}
