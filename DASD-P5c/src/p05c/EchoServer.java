package p05c;

import java.net.*;
import java.io.*;

class EchoServer {
	private static EchoObjectStub eo = new EchoObjectStub();
	private static String myURL = "localhost";
	private static ServerSocket serverSocket = null;
	private static Socket clientSocket = null;
	private static BufferedReader is = null;
	private static PrintWriter os = null;
	private static String inputline = new String();

	public static void main(String[] args) {
		try {
			myURL = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			System.out.println("Host desconegut: " + e.toString());
			System.exit(1);
		}
		try {
			serverSocket = new ServerSocket(4000);
		} catch (IOException e) {
			System.out.println(myURL + ": no pot obrir pel port 4000, " + e.toString());
			System.exit(1);
		}
		System.out.println(myURL + ": EchoServer escoltant pel port 4000");
		try {
			boolean escoltant = true;
			while (escoltant) {
				clientSocket = serverSocket.accept();
				is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				os = new PrintWriter(clientSocket.getOutputStream());
				while ((inputline = is.readLine()) != null) {
					System.out.println(inputline);
					os.println(inputline);
					os.flush();
				}
			}
			os.close();
			is.close();
			clientSocket.close();
			serverSocket.close();
		} catch (IOException e) {
			System.err.println("Error enviant/rebent" + e.getMessage());
			e.printStackTrace();
		}
	}
}
