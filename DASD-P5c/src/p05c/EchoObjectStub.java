package p05c;

import java.net.*;
import java.io.*;

public class EchoObjectStub implements EchoInt {
	private Socket echoSocket = null;
	private PrintWriter os = null;
	private BufferedReader is = null;
	private String host = "localhost";
	private int port = 7;
	private String output = "Error";
	private boolean connectat = false;

	public void setHostAndPort(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public String echo(String input) throws java.rmi.RemoteException {
		connecta();
		if (echoSocket != null && os != null && is != null) {
			try {
				os.println(input);
				os.flush();
				output = is.readLine();
			} catch (IOException e) {
				System.err.println("Excepció llegint/escrivint socket");
			}
		}
		desconnecta();
		return output;
	}

	private synchronized void connecta() throws java.rmi.RemoteException {
		try {
			if (!connectat) {
				/* COMPLETAR: Genere echoSocket i assigne is i os */
				/* 1 */
				echoSocket = new Socket(host, port);
				is = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
				os = new PrintWriter(echoSocket.getOutputStream());
				/* 1 */
				connectat = true;
				System.out.println("Connectat al server");
			}
		} catch (Exception e) {
			throw new java.rmi.RemoteException();
		}
	}

	private synchronized void desconnecta() {
		try {
			/* COMPLETAR: Tanque les connexions necessàries */
			/* 2 */
			echoSocket.close();
			is.close();
			os.close();
			/* 2 */
			connectat = false;
			System.out.println("Desconnectat del server");
		} catch (IOException e) {
			System.err.println("Error tancant socket");
		}
	}
}
