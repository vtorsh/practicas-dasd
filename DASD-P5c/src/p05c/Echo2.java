package p05c;

import java.io.*;
import java.net.*;

public class Echo2 {
	private static EchoObjectStub ss;

	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Us: Echo2 host port");
			System.exit(1);
		}
		/*
		 * COMPLETAR: Cree una instància de ss i invoque ss.setHostAndPort de
		 * manera que faça ús del host i port passats com arguments
		 */
		/* 1 */
		ss = new EchoObjectStub();
System.out.println("Argumento 1: " + args[0] + "\nArgumento 2: " + args[1]);
		ss.setHostAndPort(args[0], Integer.parseInt( args[1]) );
		/* 1 */
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter stdOut = new PrintWriter(System.out);
		String input, output;
		try {
			stdOut.print("> ");
			stdOut.flush();
			while ((input = stdIn.readLine()) != null) {
				output = ss.echo(input);
				stdOut.println(output);
				stdOut.print("> ");
				stdOut.flush();
			}
		} catch (IOException e) {
		}
	}
}
